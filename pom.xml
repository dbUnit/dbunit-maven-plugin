<?xml version="1.0" encoding="ISO-8859-1"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>org.sonatype.oss</groupId>
    <artifactId>oss-parent</artifactId>
    <version>7</version>
    <relativePath />
  </parent>

  <groupId>org.dbunit</groupId>
  <artifactId>dbunit-maven-plugin</artifactId>
  <version>1.2.1-SNAPSHOT</version>
  <packaging>maven-plugin</packaging>

  <name>dbUnit Maven Plugin</name>
  <url>http://dbunit.sourceforge.net/dbunit-maven-plugin/</url>
  <inceptionYear>2006</inceptionYear>
  <description>
    Use this plugin to invoke dbUnit utilities.
  </description>

  <licenses>
    <license>
      <name>GNU Lesser General Public License, Version 2.1</name>
      <url>http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt</url>
      <distribution>repo</distribution>
    </license>
  </licenses>

  <properties>
    <!-- Maven configuration -->
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    <project.scm.id>sourceforge</project.scm.id>

    <!-- General values -->
    <compileSource>1.8</compileSource>
    <mavenVersion>3.0.4</mavenVersion>

    <!-- Framework versions -->
    <antVersion>1.6.5</antVersion>
    <!-- TODO this needed for plugin? -->
    <commonsCollectionsVersion>3.2.2</commonsCollectionsVersion>
    <!-- always set to minimum version this plugin needs for its features -->
    <dbunitVersion>2.7.3</dbunitVersion>
    <junitVersion>4.12</junitVersion>
    <mavenPluginApiVersion>3.3.9</mavenPluginApiVersion>
    <mavenSettingsVersion>3.3.9</mavenSettingsVersion>
    <slf4jVersion>1.7.25</slf4jVersion>

    <!-- Database driver versions -->
    <hsqldbDriverVersion>1.8.0.7</hsqldbDriverVersion>

    <!-- Maven plugin versions -->
    <mavenChangesPluginVersion>2.12</mavenChangesPluginVersion>
    <compilerPluginVersion>3.7.0</compilerPluginVersion>
    <javadocPluginVersion>3.0.0</javadocPluginVersion>
    <mavenInvokerPluginVersion>2.0.0</mavenInvokerPluginVersion>
    <mavenPluginPluginVersion>2.5</mavenPluginPluginVersion>
    <projectInfoReportsPluginVersion>2.9</projectInfoReportsPluginVersion>
    <sitePluginVersion>3.7.1</sitePluginVersion>
    <sourcePluginVersion>3.0.1</sourcePluginVersion>
    <versionsMavenPluginVersion>2.8.1</versionsMavenPluginVersion>

    <!-- Other versions -->
    <wagonSshVersion>2.10</wagonSshVersion>
  </properties>

  <scm>
    <connection>scm:git:http://git.code.sf.net/p/dbunit/dbunit-maven-plugin.git</connection>
    <developerConnection>scm:git:https://git.code.sf.net/p/dbunit/dbunit-maven-plugin.git</developerConnection>
    <url>https://sourceforge.net/p/dbunit/dbunit-maven-plugin/ci/master/tree/</url>
    <tag>HEAD</tag>
  </scm>
  <issueManagement>
    <system>SourceForge2</system>
    <url>http://sourceforge.net/p/dbunit/plugintickets/</url>
  </issueManagement>
  <ciManagement>
    <system>gitlab</system>
    <url>https://gitlab.com/dbUnit/dbunit-maven-plugin/-/pipelines</url>
  </ciManagement>

  <mailingLists>
    <mailingList>
      <name>dbUnit User List</name>
      <subscribe>http://lists.sourceforge.net/lists/listinfo/dbunit-user</subscribe>
      <unsubscribe>http://lists.sourceforge.net/lists/listinfo/dbunit-user</unsubscribe>
      <archive>http://sourceforge.net/mailarchive/forum.php?forum_name=dbunit-user</archive>
    </mailingList>
    <mailingList>
      <name>dbUnit Developer List</name>
      <subscribe>http://lists.sourceforge.net/lists/listinfo/dbunit-developer</subscribe>
      <unsubscribe>http://lists.sourceforge.net/lists/listinfo/dbunit-developer</unsubscribe>
      <archive>http://sourceforge.net/mailarchive/forum.php?forum_name=dbunit-developer</archive>
    </mailingList>
    <mailingList>
      <name>dbUnit Commit List</name>
      <subscribe>http://lists.sourceforge.net/lists/listinfo/dbunit-commit</subscribe>
      <unsubscribe>http://lists.sourceforge.net/lists/listinfo/dbunit-commit</unsubscribe>
      <archive>http://sourceforge.net/mailarchive/forum.php?forum_name=dbunit-commit</archive>
    </mailingList>
  </mailingLists>

  <prerequisites>
    <maven>2.0</maven>
  </prerequisites>

  <developers>
    <developer>
      <name>Jeff Jensen</name>
      <id>jeffjensen</id>
      <email>jeffjensen@users.sourceforge.net</email>
      <roles>
        <role>Java Developer (active)</role>
      </roles>
    </developer>
    <developer>
      <id>topping</id>
      <name>Brian Topping</name>
      <email>topping@codehaus.org</email>
      <roles>
        <role>Java Developer</role>
      </roles>
      <timezone>+8</timezone>
    </developer>
    <developer>
      <id>dtran</id>
      <name>Dan Tran</name>
      <email>dantran@apache.org</email>
      <roles>
        <role>Java Developer</role>
      </roles>
    </developer>
    <developer>
      <id>david</id>
      <name>David J. M. Karlsen</name>
      <email>david@codehaus.org</email>
      <organization>codehaus.org</organization>
      <organizationUrl>http://www.codehaus.org</organizationUrl>
      <roles>
        <role>Java Developer</role>
      </roles>
      <timezone>1</timezone>
      <url>http://www.davidkarlsen.com</url>
    </developer>
  </developers>

  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>${junitVersion}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>hsqldb</groupId>
      <artifactId>hsqldb</artifactId>
      <version>${hsqldbDriverVersion}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.dbunit</groupId>
      <artifactId>dbunit</artifactId>
      <version>${dbunitVersion}</version>
    </dependency>
    <!-- dbunit depends on slf4j -->
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-api</artifactId>
      <version>${slf4jVersion}</version>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-simple</artifactId>
      <version>${slf4jVersion}</version>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <groupId>ant</groupId>
      <artifactId>ant</artifactId>
      <version>${antVersion}</version>
    </dependency>
    <dependency>
      <groupId>org.apache.maven</groupId>
      <artifactId>maven-plugin-api</artifactId>
      <version>${mavenPluginApiVersion}</version>
    </dependency>
    <dependency>
      <groupId>org.apache.maven</groupId>
      <artifactId>maven-settings</artifactId>
      <version>${mavenSettingsVersion}</version>
    </dependency>
    <dependency>
      <groupId>commons-collections</groupId>
      <artifactId>commons-collections</artifactId>
      <version>${commonsCollectionsVersion}</version>
    </dependency>
  </dependencies>

  <build>
    <defaultGoal>install</defaultGoal>

    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-javadoc-plugin</artifactId>
          <version>${javadocPluginVersion}</version>
          <!--
            FIXME remove -Xdoclint:none after JavaDoc cleanup.
          -->
          <configuration>
            <additionalparam>-Xdoclint:none</additionalparam>
            <doclint>none</doclint>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-plugin-plugin</artifactId>
          <version>${mavenPluginPluginVersion}</version>
          <executions>
            <execution>
              <id>help-mojo</id>
              <goals>
                <goal>helpmojo</goal>
              </goals>
            </execution>
          </executions>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-site-plugin</artifactId>
          <version>${sitePluginVersion}</version>
          <dependencies>
            <dependency>
              <groupId>org.apache.maven.wagon</groupId>
              <artifactId>wagon-ssh</artifactId>
              <version>${wagonSshVersion}</version>
            </dependency>
          </dependencies>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-source-plugin</artifactId>
          <version>${sourcePluginVersion}</version>
          <executions>
            <execution>
              <id>attach-sources</id>
              <phase>verify</phase>
              <goals>
                <goal>jar-no-fork</goal>
              </goals>
            </execution>
          </executions>
        </plugin>
        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>versions-maven-plugin</artifactId>
          <version>${versionsMavenPluginVersion}</version>
          <configuration>
            <rulesUri>file:///${project.basedir}/versions-maven-plugin-rules.xml</rulesUri>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>

    <plugins>
      <!--
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-docck-plugin</artifactId>
        <executions>
          <execution>
            <goals>
              <goal>check</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
       -->

      <plugin>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>${compilerPluginVersion}</version>
        <configuration>
          <source>${compileSource}</source>
          <target>${compileSource}</target>
          <compilerVersion>${compileSource}</compilerVersion>
          <encoding>${project.build.sourceEncoding}</encoding>
        </configuration>
      </plugin>
      <plugin>
        <artifactId>maven-invoker-plugin</artifactId>
        <version>${mavenInvokerPluginVersion}</version>
        <configuration>
          <debug>true</debug>
          <projectsDirectory>src/example</projectsDirectory>
          <localRepositoryPath>${project.build.directory}/it-repo</localRepositoryPath>
          <pomIncludes>
            <pomInclude>**/pom.xml</pomInclude>
          </pomIncludes>
          <cloneProjectsTo>${project.build.directory}/it</cloneProjectsTo>
          <settingsFile>src/it/settings.xml</settingsFile>
        </configuration>
        <executions>
          <execution>
            <id>integration-test</id>
            <goals>
              <goal>install</goal>
              <goal>run</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-source-plugin</artifactId>
      </plugin>
    </plugins>
  </build>

  <reporting>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-changes-plugin</artifactId>
        <version>${mavenChangesPluginVersion}</version>
        <configuration>
          <smtpHost>localhost</smtpHost>
          <smtpPort implementation="java.lang.Integer">25</smtpPort>
          <introduction>If you are reading this, the maintainer forgot to describe what's the purpose of this release!!!</introduction>
          <toAddresses>
            <toAddress implementation="java.lang.String">dbunit-developer@lists.sourceforge.net</toAddress>
            <toAddress implementation="java.lang.String">dbunit-user@lists.sourceforge.net</toAddress>
          </toAddresses>
          <urlDownload>http://dbunit.sourceforge.net/repos.html</urlDownload>
          <issueLinkTemplatePerSystem>
            <sftickets><![CDATA[https://sourceforge.net/p/dbunit/plugintickets/%ISSUE%]]></sftickets>
            <sfmerges><![CDATA[https://sourceforge.net/p/dbunit/dbunit-maven-plugin/merge-requests/%ISSUE%]]></sfmerges>
          </issueLinkTemplatePerSystem>
        </configuration>
        <reportSets>
          <reportSet>
            <reports>
              <report>changes-report</report>
            </reports>
          </reportSet>
        </reportSets>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-plugin-plugin</artifactId>
        <version>${mavenPluginPluginVersion}</version>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-project-info-reports-plugin</artifactId>
        <version>${projectInfoReportsPluginVersion}</version>
      </plugin>
    </plugins>
  </reporting>

  <distributionManagement>
    <!-- distribution repos specified in parent -->
    <site>
      <id>sourceforge</id>
      <url>scp://shell.sourceforge.net/home/project-web/dbunit/htdocs/dbunit-maven-plugin/</url>
    </site>
  </distributionManagement>
</project>
