 ----
 How to Release
 ----

Releasing

* Overview

 The following steps describe how to release the dbUnit Maven Plugin.  
 While the Maven release plugin automates steps such as version change, commit, and deploy,
 the dbUnit Maven Plugin has manual file information update steps that are outside of its scope.
 
 While the below steps do not use the Maven release plugin, 
 you can use it by performing the manual file edits first then running the release plugin steps.

* Release Steps

 [[1]] Update pom.xml

   [[a]] Set version to release version
  
 [[1]] Update changes.xml

   [[a]] Update release version

   [[a]] Set release date to today

 [[1]] Commit files "Prep release x.y.z"

 [[1]] Tag the commit; prefix release number with "plugin-"

 [[1]] Push the commit

 [[1]] Build and deploy the release

---
mvn clean
mvn deploy -Psonatype-oss-release
---

 [[1]] Login to Sonatype OSS and process the release through staging
 (requires privileges)

 [[1]] Build and deploy the site

   [[i]] Build site:

---
mvn site
---

   [[i]] Review site results for issues and correct as necessary
   
   [[i]] Deploy to SourceForge;
   reference how-to: 
   {{{http://maven.apache.org/plugins/maven-site-plugin/examples/site-deploy-to-sourceforge.net.html}Deploying to sourceforge.net}}

---
ssh -t sf-username,dbunit@shell.sourceforge.net create
mvn site:deploy
---

 [[1]] Generate Announcement Email

---
mvn changes:announcement-generate -Dchanges.version=theNewVersion
---
 Review and adjust the text as desired before using.

 [[1]] Email Users List

 [[1]] Post News on SourceForge

 [[1]] Prepare next release in SCM

   [[i]] Update pom.xml

     [[i]] Update to SNAPSHOT version

   [[i]] Update src/example/pom.xml

     [[i]] Update to match new plugin (SNAPSHOT) version

   [[i]] Update changes.xml with new SNAPSHOT entry
